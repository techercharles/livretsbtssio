<#
.SYNOPSIS
Generation des livrets scolaires du BTS SIO des candidats a partir de leur resultats scolaires de 1re et 2e annee 
.DESCRIPTION
    Ce script genere les livrets scolairs sous forme de Documents Excel a partir d'un document Excel modele.
    Les donnees des candidats et de leurs moyennes semestrielles de 1re annee et de 2e annee sont dans des documents Excel qu'il faut preparer au prealable.
    Il est imperatif de respecter la nomenclature de noms de fichiers Excel, des onglets des documents Excel et des noms des colonnes de chaque feuille Excel.
    La gestion des documents Excel est realisee avec le module ImportExcel : https://www.powershellgallery.com/packages/ImportExcel/
    Le script utilise comme parametre le nom de la classe. Par defaut c'est BTSSIO. Tous les documents Excel doivent alors etre prefixes par ce nom (BTSSIO par defaut)  
    .EXAMPLE
    ./livretbtssio.ps1 -Classe BTSSIO
    .LINK
    https://forge.apps.education.fr/techercharles/livretsbtssio
    .DEPOTGITLAB
    https://forge.apps.education.fr/techercharles/livretsbtssio.git
#>
# Parametre $Classe de la ligne de commande : nom du sous-dossier pour les livrets avec BTSSIO comme valeur par defaut BTSSIO
param(
     [Parameter()]
     [string]$Classe = "BTSSIO"
 )
 
# Fonction pour l'ecriture des logs
function New-LigneLog {
   param(
      [string]$Message
   )
   Add-Content -Path 'livretsbtssio.log' -Value ((Get-Date -Format "MM/dd/yyyy HH:mm") +  " : $Message")
   Write-Host $Message
}

# Fonction pour le calcul des moyennes semestrielles
function Get-Moyenne {
   param(
      $note1,
      $note2
   )
   
   if(($note1 -is [double]) -and ($note2 -is [double])) {
      ($note1 + $note2)/2
   } else {
      if(($note1 -is [double])) {
         $note1
      } else {
         if(($note2 -is [double])) {
            $note2
         } else {""}
      }
   }
}

#verification de l'existance du fichier de log livretsbtssio.log 
if (!(Get-ChildItem -Filter 'livretsbtssio.log') ) {New-Item -Name 'livretsbtssio.log' -ItemType File}

#creation dossier BTSSIO s'il n'existe pas
if (!(Get-ChildItem -Filter ${Classe}) ) {New-Item -Name ${Classe} -ItemType Directory}
New-LigneLog -Message "Dossier $Classe cree"


#Arreter le script si le modele de livret est absent
if (!(Get-ChildItem -Filter "${Classe}_Livretscolaire_Modele.xlsx") ) {
  # arret du script
  Write-Host "*****************************************************************************************"
  Write-Host "*********** Arret du script => le fichier modele n'existe pas dans le dossier ***********"
  Write-Host "*****************************************************************************************"
  New-LigneLog -Message "Fichier modele Livret ${Classe}_Livretscolaire_Modele.xlsx non present"
  Exit 1
  }
New-LigneLog -Message "Fichier modele Livret ${Classe}_Livretscolaire_Modele.xlsx present"

#Importation des resultats des 3 annees precedentes :  ANNEE, PPRESENTES,	RECUS,	%
$fichierresultat = Get-ChildItem -Filter ${Classe}_Resultats* 
if ($fichierresultat) {
   New-LigneLog -Message "Fichier résultat $fichierresultat trouve"
   $resultat = Import-Excel  -Path $fichierresultat 
} else {
   New-LigneLog -Message "Fichier résultat ${Classe}_Resultats non trouve"
}

#Importation des pourcentages des avis 
$fichieravis = Get-ChildItem -Filter ${Classe}_Pourcentages_AVIS* 
if ($fichieravis) {
   New-LigneLog -Message "Fichier % avis $fichieravis trouve"
   $avis = Import-Excel  -Path $fichieravis 
} else {
   New-LigneLog -Message "Fichier % avis ${Classe}_Pourcentage_AVIS non trouve"
}

#Importation des moyennes annuelles de la classe de 2e annee : 
$fichiermoyennesclasse = Get-ChildItem -Filter ${Classe}_Moyennes_CLASSE* 
if ($fichiermoyennesclasse) {
   New-LigneLog -Message "Fichier des moyennes annuelles $fichiermoyennesclasse trouve"
   $moyennesclasse = Import-Excel  -Path $fichiermoyennesclasse  
} else {
   New-LigneLog -Message "Fichier des moyennes annuelles ${Classe}_Moyennes_CLASSE non trouve"
}

# Creation des livrets si necessaire
# Rechercher s'il existe des fichiers qui commencent par BTSSIO_Identite
$listefichier = Get-ChildItem -Filter ${Classe}_Identite* 

if ($listefichier) {
   foreach ($nomfichier in $listefichier) {
      #Importer fichier identite contenant :  nom, date de naissance, option (SISR ou SLAM), resultat PIX (Oui ou rien), avis du Conseil de classe
      New-LigneLog -Message "Fichier Identité $nomfichier trouve"
      $identite = Import-Excel  -Path $nomfichier 

      foreach ($etu in $identite) {
         if (($etu.NOM -ne 'NOM') -and $etu.NOM) {
            $nomfichier = "${Classe}_livret_" + $etu.NOM.replace(' ','_') + ".xlsx"

            # extraire le nom ecrit en lettre majuscule
            for($i = 0; $i -lt $etu.NOM.Length;$i++) {
               if ($etu.NOM.Substring($i,1) -cmatch "[a-zâàäâéèëâôû]") {
                  $nom = $etu.NOM.Substring(0,$i - 2)
                  $prenom = $etu.NOM.Substring($i - 1)
                  break
               }
            }
            $datenais = $etu.DATENAIS.ToString("dd/MM/yyy") 
            $option = $etu.OPTION
            $pix = $etu.PIX

            # creation livret a partir du modele 'BTSSIO_Livretscolaire_Modele.xlsx' s'il n'existe pas 
            if (!(Get-ChildItem -Filter "${Classe}/$nomfichier") ) {
               Copy-Item -Path "${Classe}_Livretscolaire_Modele.xlsx" -Destination "${Classe}/$nomfichier"
               New-LigneLog -Message "Fichier Livret $nomfichier cree"
            }
            # ouvrir fichier existant ou nouvellement cree et mise à jour de l'onglet Recto
            $excel = Open-ExcelPackage -Path "${Classe}/$nomfichier"
            $onglet = $excel.Workbook.Worksheets['Recto']
            $onglet.Cells['H1'].Value = "NOM : $nom"
            $onglet.Cells['H3'].Value = $prenom          
            $onglet.Cells['K3'].Value = $datenais

            if ($option -eq 'SISR') {
              $caseoptionSISR = "X"
              $caseoptionSLAM = "▢"
            }
            else {
              $caseoptionSISR = "▢"
              $caseoptionSLAM = "X"
            }
            $onglet.Cells['D3'].Value =   "$caseoptionSISR Solutions d’infrastructure, systèmes et réseaux (SISR) `r "
            $onglet.Cells['D3'].Value +=  "$caseoptionSLAM Solutions logicielles et applications métiers (SLAM)"
            if ($pix -eq 'OUI') {
              $onglet.Cells['E20'].Value = "X"
            }
            else {
              $onglet.Cells['E20'].Value = "▢"
            }

            # Ajout des resultats des annees precedentes :  ANNEE, PRESENTES,	RECUS,	POURCENTAGE
            $resultat | ForEach-Object -Begin {$i = 0} -Process {
                                  if ($i -lt 3) {
                                     $onglet.SetValue($i + 24,  8, $PSItem.ANNEE)
                                     $onglet.SetValue($i + 24,  9, $PSItem.PRESENTES)
                                     $onglet.SetValue($i + 24, 10, $PSItem.RECUS)
                                     $onglet.SetValue($i + 24, 11, $PSItem.POURCENTAGE)
                                     $i++
                                   }
                               }
            # Ajout de l'avis du Conseil de classe
            $onglet.Cells['A23'].Value  = $etu.AVIS           

            # Ajout des pourcentages des avis
            $onglet.Cells['E25'].Value  = $avis.TF
            $onglet.Cells['F25'].Value  = $avis.F
            $onglet.Cells['G25'].Value  = $avis.DP
                 
            # Ajout des moyennes annuelles de la classe pour le tracet des courbes
            $onglet.Cells['N6'].Value  = $moyennesclasse.CGEN
            $onglet.Cells['N7'].Value  = $moyennesclasse.ANGLAIS
            $onglet.Cells['N8'].Value  = $moyennesclasse.MATHS
            $onglet.Cells['N9'].Value  = $moyennesclasse.CEJM
            $onglet.Cells['N10'].Value = $moyennesclasse.CEJMA
            $onglet.Cells['N11'].Value = $moyennesclasse.BLOC1
            $onglet.Cells['N12'].Value = $moyennesclasse.BLOC2
            $onglet.Cells['N13'].Value = $moyennesclasse.BLOC3
         
            Close-ExcelPackage $excel

            New-LigneLog -Message "Fichier Livret $nomfichier mis à jour"
         }
      }
   }
} else {
   New-LigneLog -Message "Fichier Identité ${Classe}_Identite non trouve"
}

# Ajout des moyennes de 1re et 2e annee
# Rechercher s'il existe des fichiers qui commencent par ${Classe}_Moyennes_SIO
$listefichiermoyennes = Get-ChildItem -Filter "${Classe}_Moyennes_SIO*" 

if ($listefichiermoyennes) {
   foreach ($nomfichiermoyenne in $listefichiermoyennes) {
      # determiner s'il s'agit d'un fichier des moyennes de 1re annee ou de 2e annee pour gerer decalage du numero de colonne a utiliser
      if($nomfichiermoyenne.ToString().IndexOf("SIO1") -ne -1) {
         # parametrage pour positionner la moyenne (decalMoy) ainsi que la moyenne annuelle (decalMoyAn) de 1re annee
         $parMoyenne = @{annee = 1 ; decalMoy = 1 ; decalMoyAn = 3}
       } else {
         # parametrage pour positionner la moyenne (decalMoy) ainsi que la moyenne annuelle (decalMoyAn) de 2e annee
         $parMoyenne = @{annee = 2 ; decalMoy = 8 ; decalMoyAn = 10}
       }

      #Importer le fichier des moyennes 
      New-LigneLog -Message "Fichier des moyennes $nomfichiermoyenne trouve"
      $data = @('SEMESTRE1','SEMESTRE2') 
      for ($i = 0; $i -lt 2 ; $i++) {  
         # importation semestre 
         $moyenne = Import-Excel  -Path $nomfichiermoyenne -WorksheetName $data[$i]
         foreach ($etu in $moyenne) {
           if ($etu.NOM -ne 'NOM') {
              $nom = $etu.NOM
              $nomfichieretu = "${Classe}_livret_" + $etu.NOM.replace(' ','_') + ".xlsx"
              if (!(Get-ChildItem -Filter "${Classe}/$nomfichieretu") ) {
                 New-LigneLog -Message "Fichier livret $nomfichieretu non present pour l'ajout des moyennes"
              } else {
                 # ouvrir fichier livret et mise à jour des moyennes
                 $excel = Open-ExcelPackage -Path "${Classe}/$nomfichieretu"
                 $onglet = $excel.Workbook.Worksheets['Recto']
                 # insertion des moyennes semestrielles
                 $onglet.SetValue( 6,($i + $parMoyenne['decalMoy']),$etu.CGEN)
                 $onglet.SetValue( 7,($i + $parMoyenne['decalMoy']),$etu.ANGLAIS)
                 $onglet.SetValue( 8,($i + $parMoyenne['decalMoy']),$etu.MATHS)
                 $onglet.SetValue( 9,($i + $parMoyenne['decalMoy']),$etu.CEJM)
                 $onglet.SetValue(10,($i + $parMoyenne['decalMoy']),$etu.CEJMA)
                 $onglet.SetValue(11,($i + $parMoyenne['decalMoy']),$etu.BLOC1)
                 $onglet.SetValue(12,($i + $parMoyenne['decalMoy']),$etu.BLOC2)
                 $onglet.SetValue(13,($i + $parMoyenne['decalMoy']),$etu.BLOC3)
                 $onglet.SetValue(16,($i + $parMoyenne['decalMoy']),$etu.LV2)
                 $onglet.SetValue(17,($i + $parMoyenne['decalMoy']),$etu.MATHSAPPRO)
                 $onglet.SetValue(18,($i + $parMoyenne['decalMoy']),$etu.CERTIFICATION)

                 # insertion des moyennes annuelles
                 $onglet.SetValue( 6,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue( 6,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue( 6,$parMoyenne['decalMoyAn'] - 1))) # CGEN
                 $onglet.SetValue( 7,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue( 7,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue( 7,$parMoyenne['decalMoyAn'] - 1))) # ANGLAIS
                 $onglet.SetValue( 8,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue( 8,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue( 8,$parMoyenne['decalMoyAn'] - 1))) # MATHS
                 $onglet.SetValue( 9,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue( 9,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue( 9,$parMoyenne['decalMoyAn'] - 1))) # CEJM
                 $onglet.SetValue(10,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue(10,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue(10,$parMoyenne['decalMoyAn'] - 1))) # CEJMA 
                 $onglet.SetValue(11,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue(11,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue(11,$parMoyenne['decalMoyAn'] - 1))) # BLOC 1
                 $onglet.SetValue(12,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue(12,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue(12,$parMoyenne['decalMoyAn'] - 1))) # BLOC 2
                 $onglet.SetValue(13,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue(13,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue(13,$parMoyenne['decalMoyAn'] - 1))) # BLOC 3
                 $onglet.SetValue(16,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue(16,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue(16,$parMoyenne['decalMoyAn'] - 1))) # LV2
                 $onglet.SetValue(17,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue(17,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue(17,$parMoyenne['decalMoyAn'] - 1))) # MATHS APPRO
                 $onglet.SetValue(18,$parMoyenne['decalMoyAn'],(Get-Moyenne -note1 $onglet.GetValue(18,$parMoyenne['decalMoyAn'] - 2) -note2 $onglet.GetValue(18,$parMoyenne['decalMoyAn'] - 1))) # CERTIFICATION
                                                     
                 Close-ExcelPackage $excel
                 $message = "Moyennes semestre $($i+1) annee " + $parMoyenne['annee'] + " renseignees pour $nom "
                 New-LigneLog -Message $message
              }
           }
         }
      }
   }
} else {
   New-LigneLog -Message "Fichier des moyennes ${Classe}_Moyennes_SIO* non trouve"
}

