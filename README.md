# livretsbtssio

## Description
Le projet consite à utiliser un script Powershell pour générer des livrets scolaires du BTS SIO des candidats à partir de leur résultats scolaires de 1re et 2e année enregistrés dans des documents tableur Excel.

Nom du script Powershell : `livretsbtssio.ps1`

La gestion des documents tableur utilise le module ImportExcel : https://www.powershellgallery.com/packages/ImportExcel/

## Installation
Le script est écrit en Powershell, disponible dans les OS Windows client et serveur avec l'édition Desktop. 

Je vous conseille d'utiliser la version Powershell Core qui est multi-plateforme (Windows / Linux / Mac OS X) afin de ne pas avoir de problème d'encodage des caractères. Par aileurs cette version Powershell Core évolue régulièrement (version 7.4.2 en avril 2024) à la différence de l'édition Desktop (version 5.1 en avril 2024).

Lien vers la page de Microsoft Learn pour l'installattion de l'édition Powershell Core :
https://learn.microsoft.com/fr-fr/powershell/scripting/install/installing-powershell. 

Pour l'OS Windows, l'exécution des scripts Powershell est désactivée par défaut. il est nécessaire d'autoriser au préalable l'exécution des scripts Powershell non signés. 

Utilisez la commande suivante avec Powershell en tant qu'administrateur  :

```powershell
PS > Set-ExecutionPolicy Unrestricted
```

Pour revenir à la stratégie par défaut en désactivant l'exécution des scripts Powershell. 

Utilisez la commande suivante avec Powershell en tant qu'administrateur  : 

```powershell
PS > Set-ExecutionPolicy Rectrited
```

Le script Powershell utilise le module ImportExcel qu'il faut installer : https://www.powershellgallery.com/packages/ImportExcel

Installation du module ImportExcel :

```powershell
PS > Install-module -Name ImportExcel
PS > Install-PSResource -Name ImportExcel
```
## Préparation des données
Afin de pouvoir gérer plusieurs classes ou permettre de distinguer les livrets des candidats de l'option SISR ou SLAM, le script utilise un paramètre `classe` qui est renseigné par défaut avec la valeur `BTSSIO`.

Il est `imperatif` de respecter la nomenclature suivante pour les noms des documents tableur, les onglets des documents tableur et les noms des colonnes de chaque onglet tableur.

  * Toutes les données nécessaires à l'exécution du script doivent être préfixés avec le `nom de la classe` (nom BTSSIO par défaut). Exemple : **BTSSIO_Identite.xlsx**
  * Le document tableur `modèle de livret` des candidats est celui de la circulaire nationale et doit être nommé `classe_Livretscolaire_Modele.xlsx`. Exemple **BTSSIO_Livretscolaire_Modele.xlsx**  
  * Les `données des résultats des années précédentes` du BTS SIO doivent être renseignées dans un seul fichier Excel préfixé `classe_Resultats`. Exemple **BTSSIO_Resultats.xlsx**  
    * Les `données des moyennes annuelles de la classe de 2e année` du BTS SIO doivent être renseignées dans un seul fichier Excel préfixé `classe_Moyennes_CLASSE`. Exemple **BTSSIO_Moyennes_CLASSE.xlsx**  
  * Les `données d'identité` des candidats doivent être renseignées dans des fichiers préfixés `classe_Identite`. Exemple **BTSSIO_Identite.xlsx**  
  * Les `moyennes de la 1re année du BTS SIO` des candidats doivent être renseignées dans des fichiers Excel préfixés `classe_Moyennes_SIO1`. Exemple **BTSSIO_Moyennes_SIO1.xlsx**  
  * Les `moyennes de la 2e année du BTS SIO` des candidats doivent être renseignées dans des fichiers Excel préfixés `classe_Moyennes_SIO2`. Exemple **BTSSIO_Moyennes_SIO2.xlsx**  
  * Les `pourcentages des avis de la promotion` doivent être renseignées dans un fichier Excel préfixé `TEST_Pourcentages_AVIS`. Exemple **BTSSIO_TEST_Pourcentages_AVIS.xlsx**  

Les `logs` des actions du script Powershell sont enregistrés dans le fichier `livretsbtssio.log`.

### Contenu du document tableur `classe_Resultats` 
  * Nombre d'onglets : 1
    * Onglet 1 : **Feuil1**
  * Nom des colonnes de l'onglet 1 : 
    * **ANNEE** :  année scolaire. exemple 2022-2023,
    * **PRESENTES** : nombre de candidats présentés,
    * **RECUS** : nombre de candidats présentés reçus,
    * **POURCENTAGE** : % du nombre de candidats recus.

### Contenu du document tableur `classe_Moyennes_CLASSE` 
  * Nombre d'onglets : 1
    * Onglet 1 : **Feuil1**
  * Nom des colonnes de l'onglet 1 : 
    * **CGEN**    : Moyenne semestrielle de Culture générale et expression,
    * **ANGLAIS** : Moyenne annuelle de classe de 2e année en Expression et communication en langue anglaise,
    * **MATHS**   : Moyenne annuelle de classe de 2e année en Mathématiques pour l’informatique,
    * **CEJM**    : Moyenne annuelle de classe de 2e année en Culture économique, juridique et managériale,
    * **CEJMA**   : Moyenne annuelle de classe de 2e année en Culture économique, juridique et managériale appliquée,
    * **BLOC1**   : Moyenne annuelle de classe de 2e année en Bloc 1,
    * **BLOC2**   : Moyenne annuelle de classe de 2e année en Bloc 2,
    * **BLOC3**   : Moyenne annuelle de classe de 2e année en Bloc 3.

### Contenu du document tableur `classe_Identite` 
  * Nombre d'onglets : 1
    * Onglet 1 : **Feuil1**
  * Nom des colonnes de l'onglet 1 : 
    * **NOM** :  nom (en lettre majuscule) et le prénom du candidat,
    * **DATNAIS** : date de naissance au format jj/mm/aaaa,
    * **OPTION** :  option du candidat SISR ou SLAM,
    * **PIX** : contient OUI si le candidat a obtenu la certification PIX.
    * **AVIS** : contient l'avis donné par le Conseil de classe : `Très favorable` ; `Favorable` ; `Doit faire ses preuves à l'examen`.

### Contenu des documents tableur `classe_Moyennes_SIO1` et `classe_Moyennes_SIO2` 
  * Nombre d'onglets : 2
    * Onglet 1 : **SEMESTRE1**
    * Onglet 2 : **SEMESTRE2**
  * Nom des colonnes de l'onglet SEMESTRE1 et SEMESTRE2 : 
    * **CGEN** : Moyenne semestrielle de Culture générale et expression,
    * **ANGLAIS** : Moyenne semestrielle de Expression et communication en langue anglaise,
    * **MATHS** : Moyenne semestrielle de Mathématiques pour l’informatique,
    * **CEJM** : Moyenne semestrielle de Culture économique, juridique et managériale,
    * **CEJMA** : Moyenne semestrielle de Culture économique, juridique et managériale appliquée,
    * **BLOC1** : Moyenne semestrielle du Bloc 1,
    * **BLOC2** : Moyenne semestrielle du Bloc 2 (vide au semestre 1),
    * **BLOC3** : Moyenne semestrielle du Bloc 3,
    * **LV2** : Moyenne semestrielle de Langue vivante 2,
    * **MATHSAPPRO** : Moyenne semestrielle de Mathématiques approfondies,
    * **CERTIFICATION** : Moyenne semestrielle du Parcours de certification complémentaire.

### Contenu du document tableur `classe_Pourcentage_AVIS` 
  * Nombre d'onglets : 1
    * Onglet 1 : **Feuil1**
  * Nom des colonnes de l'onglet 1 : 
    * **TF** :  Pourcentage d'avis **Très favorable**. Exemple 32.
    * **F** : Pourcentage d'avis **Favorable**,
    * **DP** : Pourcentage d'avis **Doit faire ses preuves à l'examen**.

## Usage
Les documents tableurs contenant les données doivent se trouver dans le même dossier que celui du script Powershell `livretsbtssio.ps1`.

A la fin de l'exécution du script, les livrets scolaires sont générés dans un sous-dossier au nom de la classe.

Le script peut être lancé plusieurs fois sans supprimer les appréciations saisies par les enseignants. Cela permet d'alimenter le livret au fur et à mesure de la disponibilité des données: pour les différents semestres, les avis du Conseil de classe, etc.

Lancement du script avec le nom de classe par défaut BTSSIO; tous les fichiers étant préfixés par BTSSIO : 
```powershell
PS > ./livretsbtssio.ps1
```
Lancement du script en précisant le nom de la classe TEST du jeu de test fourni; tous les fichiers étant préfixés par TEST  : 
```powershell
PS > ./livretsbtssio.ps1 -Classe TEST
```

Lancement du script en précisant le nom d'une classe, par exemple BTSSIO2 ; ; tous les fichiers étant préfixés par BTSSIO2 : 
```powershell
PS > ./livretsbtssio.ps1 -Classe BTSSIO2
```

## Support
Par couriel à charles.techer@ac-limoges.fr

## Roadmap
Il ne m'a pas été possible d'écrire le nom du candidat dans les cellules fusionnées H1:L1. J'ignore pour quelle raison. La solution de contournement a été de fusionner les cellules I1:L1 afin de pouvoir écrire le nom du candidat. Une solution est à trouver.

Les données scolaires sont issues de Pronote en utilisant la fonctionnalité de copie des données au format CSV du logiciel. Il est ensuite nécessaire de mettre en forme manuellement les données dans un logiciel tableur. 

Une des améliorations du projet consisterait à pouvoir récupérer les données directement depuis Pronote comme le permettrait l'utilisation d'une API.  

Je suis intéressé par toutes informations ou pistes de solution à ce sujet.

## Contributing
Toutes les propositions d'améliorations sont les bienvenues.

## Authors and acknowledgment
Auteur : Charles Técher

## License
MIT LIcence.

## Project status
Projet en cours d'évolution à la date d'avril 2024.
