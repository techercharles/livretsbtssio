# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2023-05-07

### Added

- Importation des avis du conseil de classe en ajoutant une colonne AVIS dans le fichier des identité classe_Identite.xlsx.
- Importation des statistiques des avis du Conseil de classe à partir d'un nouveau document Excel classe _Pourcentages_AVIS.xlsx.

### Fixed

- calcul de la moyenne anuelle en prenant en compte uniquement les moyennes semestrielles existantes.

### Changed

- calcul de ma moyenne annuelle à chaque ajout d'un moyenne semestrielle de 1re année ou de 2e année.

## [1.0.0] - 2023-04-30

### Added

- génération des livrets scolaires du BTS SIO à partir d'un scipt Powershell.


